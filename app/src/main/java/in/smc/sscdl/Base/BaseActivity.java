package in.smc.sscdl.Base;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.R;


/**
 * Created by Administrator on 2/25/2016.
 */
public class BaseActivity extends AppCompatActivity {


  protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
  protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
  public Toolbar toolbar;
  public TextView toolbarTitle;
  private AlertDialog mAlertDialog;

  /**
   * Hide alert dialog if any.
   */
  @Override
  public void onStop() {
    super.onStop();
    if (mAlertDialog != null && mAlertDialog.isShowing()) {
      mAlertDialog.dismiss();
    }
  }


  /**
   * Requests given permission.
   * If the permission has been denied previously, a Dialog will prompt the user to grant the
   * permission, otherwise it is requested directly.
   */
  protected void requestPermission(final String permission, String rationale, final int requestCode) {
    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
      showAlertDialog(getString(R.string.permission_title_rationale), rationale,
              new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  ActivityCompat.requestPermissions(BaseActivity.this,
                          new String[]{permission}, requestCode);
                }
              }, getString(R.string.label_ok), null, getString(R.string.label_cancel));
    } else {
      ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
    }
  }

  /**
   * This method shows dialog with given title & message.
   * Also there is an option to pass onClickListener for positive & negative button.
   *
   * @param title                         - dialog title
   * @param message                       - dialog message
   * @param onPositiveButtonClickListener - listener for positive button
   * @param positiveText                  - positive button text
   * @param onNegativeButtonClickListener - listener for negative button
   * @param negativeText                  - negative button text
   */
  protected void showAlertDialog(@Nullable String title, @Nullable String message,
                                 @Nullable DialogInterface.OnClickListener onPositiveButtonClickListener,
                                 @NonNull String positiveText,
                                 @Nullable DialogInterface.OnClickListener onNegativeButtonClickListener,
                                 @NonNull String negativeText) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle(title);
    builder.setMessage(message);
    builder.setPositiveButton(positiveText, onPositiveButtonClickListener);
    builder.setNegativeButton(negativeText, onNegativeButtonClickListener);
    mAlertDialog = builder.show();
  }

  public void initToolbar() {

    if (toolbar == null) {
      toolbar = (Toolbar) findViewById(R.id.toolbar);
      setSupportActionBar(toolbar);

          /*  toolbarTitle = (TextView) findViewById(R.id.toolbar_title);
            toolbarTitle.setTypeface(AppSession.getShrutiFont());

            if(AppGlobal.getIntegerPref(BaseActivity.this,AppGlobal.KEY_LANGUAGE)==0)
            toolbarTitle.setText(getResources().getString(R.string.app_name));
            else
                toolbarTitle.setText(getResources().getString(R.string.app_name_gu));*/
    }

  }

  public void setActionBarTitle(String title) {
    getSupportActionBar().setTitle(title);
  }

  public void changeBar(String title, FontAwesomeIcons icon) {
    toolBarWithIconTitle(new IconDrawable(this, icon), R.color.colorAccent, title);
  }

  public void toolBarWithIconTitle(IconDrawable iconDrawable, int colorResource, String title) {

    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setHomeAsUpIndicator(iconDrawable
              .colorRes(R.color.icons)
              .actionBarSize());
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setTitle(title);
    }
  }

  public void toolBarWithDrawableIconTitle(Drawable iconDrawable, String title) {

    final ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setHomeAsUpIndicator(iconDrawable);
      actionBar.setDisplayHomeAsUpEnabled(true);
      actionBar.setTitle(title);
    }
  }


}
