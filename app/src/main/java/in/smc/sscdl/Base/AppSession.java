package in.smc.sscdl.Base;

/**
 * Created by admin on 06/04/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;


public class AppSession {

    public static Typeface shrutiFont;
    public static SharedPreferences.Editor editor;

    public static Context acx;
    private static SharedPreferences pfs;


    private AppSession() {
    }     //enforce singleton patern


    // initialize constant values to simplify the app code
    public static void init(Context ctx) {
        acx = ctx.getApplicationContext();
        pfs = PreferenceManager.getDefaultSharedPreferences(acx);
        editor = pfs.edit();
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Typeface getShrutiFont() {
        if (shrutiFont == null) {
            shrutiFont = Typeface.createFromAsset(AppSession.acx.getAssets(), "fonts/SHRUTIB.TTF");
        }
        return shrutiFont;
    }

}
