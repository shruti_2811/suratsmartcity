package in.smc.sscdl.Base;

import android.app.Activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;

import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import in.smc.sscdl.R;


public abstract class BaseFragment extends Fragment {

    public static final int SCALE_DELAY = 30;
    private static long back_pressed;
    private View rootView;

    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    static public void replaceFragmentInContainer(Fragment DestinationFragment, int containerResourceID, FragmentManager fragmentManager) {

        // fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentTransaction ft = fragmentManager.beginTransaction();
        int viewResourceID = containerResourceID;
        ft.replace(viewResourceID, DestinationFragment);
        //ft.addToBackStack(null);
        ft.commit();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRootView(view);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        hide_keyboard(getActivity());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public View getRootView() {
        return rootView;
    }

    public void setRootView(View rootView) {
        this.rootView = rootView;
    }

    protected int getActionBarSize() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        return activity.findViewById(android.R.id.content).getHeight();
    }

    public void pushFragment(Fragment DestinationFragment) throws Exception {
        try {
            Fragment SourceFragment = this;
            int viewResourceID = ((ViewGroup) SourceFragment.getView().getParent()).getId();
            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            ft.add(viewResourceID, DestinationFragment);
            ft.hide(SourceFragment);
            ft.commit();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void pushFragmentWithBackState(Fragment DestinationFragment, boolean isAnimate) throws Exception {
        try {
            Fragment SourceFragment = this;
            int viewResourceID = ((ViewGroup) SourceFragment.getView().getParent()).getId();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (isAnimate) {
                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            }
            ft.add(viewResourceID, DestinationFragment);
            ft.hide(SourceFragment);

            ft.addToBackStack(DestinationFragment.getClass().getName());
            ft.commit();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void pushFragmentWithPagerBackState(int id, Fragment DestinationFragment) throws Exception {
        try {
            FragmentTransaction trans = getFragmentManager().beginTransaction();
            trans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            trans.replace(id, DestinationFragment);
            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            trans.addToBackStack(null);

            trans.commit();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void pushFragmentWithBackState(Fragment DestinationFragment, int viewResourceID) throws Exception {
        try {
            Fragment SourceFragment = this;
            FragmentManager fragmentManager = getFragmentManager();

            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            ft.add(viewResourceID, DestinationFragment);
            ft.hide(SourceFragment);
            ft.addToBackStack(SourceFragment.getClass().getName());
            ft.commit();
        } catch (Exception ex) {
            throw ex;
        }

    }

    public void replaceFragment(Fragment DestinationFragment) {
        Fragment SourceFragment = this;
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        int viewResourceID = ((ViewGroup) SourceFragment.getView().getParent()).getId();
        ft.replace(viewResourceID, DestinationFragment);
        ft.commit();
    }

    public boolean popFragment(Fragment SourceFragment) {
        getFragmentManager().popBackStack();
        return true;
    }

    public boolean popChildFragment() {
        getChildFragmentManager().popBackStack();
        return true;
    }

    public boolean poptoBackStackFragment(Class SourceFragmentClass) {
        getFragmentManager().popBackStack(SourceFragmentClass.getName(), 0);
        return true;
    }


    public boolean onFragmentBackPressed() {

        int Count = getActivity().getSupportFragmentManager().getBackStackEntryCount();

        if (Count == 0) {
            if (back_pressed + 2000 > System.currentTimeMillis()) {
                getActivity().finish();
            } else {
                Toast.makeText(getActivity().getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
                back_pressed = System.currentTimeMillis();
            }
            return true;
        } else {
            return popFragment(this);
        }
    }


}

