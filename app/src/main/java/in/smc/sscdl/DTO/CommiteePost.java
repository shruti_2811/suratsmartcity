package in.smc.sscdl.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommiteePost {
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Msg")
    @Expose
    private String msg;
    @SerializedName("committee")
    @Expose
    public Committee committee;

    public static class Committee {
        @SerializedName("hr")
        @Expose
        public List<Hr> hr = new ArrayList();
        @SerializedName("pmc")
        @Expose
        public List<Pmc> pmc = new ArrayList();
        @SerializedName("audit")
        @Expose
        public List<Audit> audit = new ArrayList();
        @SerializedName("csr")
        @Expose
        public List<Csr> csr = new ArrayList();
        @SerializedName("complaint")
        @Expose
        public List<Complaint> complaint = new ArrayList();



        public class Complaint {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("designation")
            @Expose
            private String designation;

            public String getId() {
                return id;
            }



            public String getName() {
                return name;
            }



            public String getDesignation() {
                return designation;
            }



        }

        public class Audit {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("designation")
            @Expose
            private String designation;

            public String getId() {
                return id;
            }



            public String getName() {
                return name;
            }



            public String getDesignation() {
                return designation;
            }


        }

        public class Csr {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("designation")
            @Expose
            private String designation;

            public String getId() {
                return id;
            }


            public String getName() {
                return name;
            }



            public String getDesignation() {
                return designation;
            }



        }

        public class Hr {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("designation")
            @Expose
            private String designation;


            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }



            public String getDesignation() {
                return designation;
            }
        }

        public class Pmc {

            @SerializedName("id")
            @Expose
            private String id;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("designation")
            @Expose
            private String designation;

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }

            public String getDesignation() {
                return designation;
            }



        }
    }
}
