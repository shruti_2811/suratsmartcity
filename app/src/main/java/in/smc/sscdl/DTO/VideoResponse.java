package in.smc.sscdl.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VideoResponse {



    public String Status;
    public String Msg;
    public List<Videos> videos = new ArrayList();


    public static class Videos{
        @SerializedName("video-title")
        public String video_title;

        @SerializedName("video-link")
        public String video_link;

        @SerializedName("video-thumb-high")
        public String video_thumb_high;

        @SerializedName("video-thumb-medium")
        public String video_thumb_medium;

        @SerializedName("video-thumb-small")
        public String video_thumb_small;

        @SerializedName("video-embed")
        public String video_embeded;




        public Videos(String video_title, String video_link, String video_thumb_high, String video_thumb_medium, String video_thumb_small,String video_embeded) {
            this.video_title = video_title;
            this.video_link = video_link;
            this.video_thumb_high = video_thumb_high;
            this.video_thumb_medium = video_thumb_medium;
            this.video_thumb_small = video_thumb_small;
            this.video_embeded = video_embeded;
        }

        public String getVideo_embeded() {
            return video_embeded;
        }

        public void setVideo_embeded(String video_embeded) {
            this.video_embeded = video_embeded;
        }

        public String getVideo_title() {
            return video_title;
        }

        public void setVideo_title(String video_title) {
            this.video_title = video_title;
        }

        public String getVideo_link() {
            return video_link;
        }

        public void setVideo_link(String video_link) {
            this.video_link = video_link;
        }

        public String getVideo_thumb_high() {
            return video_thumb_high;
        }

        public void setVideo_thumb_high(String video_thumb_high) {
            this.video_thumb_high = video_thumb_high;
        }

        public String getVideo_thumb_medium() {
            return video_thumb_medium;
        }

        public void setVideo_thumb_medium(String video_thumb_medium) {
            this.video_thumb_medium = video_thumb_medium;
        }

        public String getVideo_thumb_small() {
            return video_thumb_small;
        }

        public void setVideo_thumb_small(String video_thumb_small) {
            this.video_thumb_small = video_thumb_small;
        }
    }
//    String videoUrl;
//
//    public VideoResponse(String videoUrl) {
//        this.videoUrl = videoUrl;
//    }
//
//    public VideoResponse() {
//
//    }
//
//    public String getVideoUrl() {
//        return videoUrl;
//    }
//
//    public void setVideoUrl(String videoUrl) {
//        this.videoUrl = videoUrl;
//    }
}
