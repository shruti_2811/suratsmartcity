package in.smc.sscdl.DTO;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AwardResponse {

    private String Status;
    private String Msg;
    public List<Award> awards = new ArrayList();

    public static class Award {

        @SerializedName("award-title")
        private String awardTitle;

        @SerializedName("award-for")
        private String awardFor;

        @SerializedName("award-by")
        private String awardBy;

        @SerializedName("award-date")

        private String awardDate;
        @SerializedName("award-photo-thumb")

        private String awardPhotoThumb;
        @SerializedName("award-photo-big")

        private String awardPhotoBig;


        public String getAwardTitle() {
            return awardTitle;
        }
        public String getAwardFor() {
            return awardFor;
        }
        public String getAwardBy() {
            return awardBy;
        }
        public String getAwardDate() {
            return awardDate;
        }
        public String getAwardPhotoThumb() {
            return awardPhotoThumb;
        }
        public String getAwardPhotoBig() {
            return awardPhotoBig;
        }

        public void setAwardPhotoBig(String awardPhotoBig) {
            this.awardPhotoBig = awardPhotoBig;
        }
        public void setAwardTitle(String awardTitle) {
            this.awardTitle = awardTitle;
        }
        public void setAwardFor(String awardFor) {
            this.awardFor = awardFor;
        }
        public void setAwardBy(String awardBy) {
            this.awardBy = awardBy;
        }
        public void setAwardDate(String awardDate) {
            this.awardDate = awardDate;
        }
        public void setAwardPhotoThumb(String awardPhotoThumb) {
            this.awardPhotoThumb = awardPhotoThumb;
        }

    }}



