package in.smc.sscdl.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by admin on 01/07/2017.
 */

public class ResObjAppProjectService implements Serializable {

    @JsonProperty("abd_services")
    private List<ObjAppService> services;


    public List<ObjAppService> getServices() {
        return services;
    }

    public void setServices(List<ObjAppService> services) {
        this.services = services;
    }
}
