package in.smc.sscdl.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by admin on 01/07/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ObjAppService implements Serializable {

    @JsonProperty("AppId")
    private int AppId;
    @JsonProperty("AppIcon")
    private String AppIcon;
    @JsonProperty("AppName")
    private String AppName;
    @JsonProperty("IsActive")
    private boolean IsActive;
    @JsonProperty("ModifyDateTime")
    private String ModifyDateTime;
    @JsonProperty("Url")
    private String Url;

    public int getAppId() {
        return AppId;
    }

    public void setAppId(int appId) {
        AppId = appId;
    }

    public String getAppIcon() {
        return AppIcon;
    }

    public void setAppIcon(String appIcon) {
        AppIcon = appIcon;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getModifyDateTime() {
        return ModifyDateTime;
    }

    public void setModifyDateTime(String modifyDateTime) {
        ModifyDateTime = modifyDateTime;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }
}
