package in.smc.sscdl.DTO;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ManagementDTO {

    private String name;
    private String desc;
    private int drawable;
    private String url;
    private String apppackage;
    public ManagementDTO(String name, String desc, int drawable) {
        this.name = name;
        this.desc = desc;
        this.drawable = drawable;
    }

    public ManagementDTO(String name, String desc, int drawable, String url, String apppackage) {
        this.name = name;
        this.desc = desc;
        this.drawable = drawable;
        this.url = url;
        this.apppackage = apppackage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApppackage() {
        return apppackage;
    }

    public void setApppackage(String apppackage) {
        this.apppackage = apppackage;
    }
}
