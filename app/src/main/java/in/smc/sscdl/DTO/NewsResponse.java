package in.smc.sscdl.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NewsResponse {
    public   String Status;
    public   String Msg;
    public List<Newslist> mediacoverage = new ArrayList();

    public class Newslist{
        @SerializedName("media-title")
        private String mediatitle;
        @SerializedName("media-date")
        private String mediadate;
        @SerializedName("media-photo-thumb")
        private String mediaphotothumb;
        @SerializedName("media-photo-big")
        private String mediaphotobig;

        public Newslist(String mediatitle, String mediadate, String mediaphotothumb, String mediaphotobig) {
            this.mediatitle = mediatitle;
            this.mediadate = mediadate;
            this.mediaphotothumb = mediaphotothumb;
            this.mediaphotobig = mediaphotobig;
        }

        public String getMediatitle() {
            return mediatitle;
        }

        public String getMediadate() {
            return mediadate;
        }

        public String getMediaphotothumb() {
            return mediaphotothumb;
        }

        public String getMediaphotobig() {
            return mediaphotobig;
        }
    }

}
