package in.smc.sscdl.DTO;

import java.util.ArrayList;
import java.util.List;

public class Post {
    public   String Status;
    public   String Msg;
    public List<PostList> management = new ArrayList();

    public class PostList {

        private  String name;
        private  String designation;
        private  String photo;

        public PostList(String name, String designation, String photo) {
            this.name = name;
            this.designation = designation;
            this.photo = photo;
        }

        public String getName() {
            return name;
        }

        public String getDesignation() {
            return designation;
        }

        public String getPhoto() {
            return photo;
        }
    }
}
