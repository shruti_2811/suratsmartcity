package in.smc.sscdl.Fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


import in.smc.sscdl.Activity.ActivityABD;
import in.smc.sscdl.Activity.ActivityAboutSMC;
import in.smc.sscdl.Activity.ActivityAboutSurat;


import in.smc.sscdl.Activity.ActivityBoardOfDirectors;
import in.smc.sscdl.Activity.ActivityContact;

import in.smc.sscdl.Activity.ActivityManagement;
import in.smc.sscdl.Activity.ActivityOurApps;
import in.smc.sscdl.Activity.ActivityPanProjects;


import in.smc.sscdl.Activity.ActivityProjects;
import in.smc.sscdl.Activity.ActivitySSDL;
import in.smc.sscdl.Activity.ActivityTenders;
import in.smc.sscdl.Activity.ActivityVision;
import in.smc.sscdl.Activity.AwardsActivity;
import in.smc.sscdl.Activity.CommiteeActivity;
import in.smc.sscdl.Activity.DataActivity;
import in.smc.sscdl.Activity.NewsActivity;
import in.smc.sscdl.Activity.VideoActivity;
import in.smc.sscdl.Base.BaseFragment;
import in.smc.sscdl.Constants.AppGlobal;
import in.smc.sscdl.Constants.Constants;
import in.smc.sscdl.DTO.ObjAppService;
import in.smc.sscdl.DTO.ResObjAppService;
import in.smc.sscdl.R;
import in.smc.sscdl.Utils.RecyclerViewSupport.RecyclerItemClickListener;
import in.smc.sscdl.Utils.RecyclerViewSupport.RecyclerViewEmptySupport;


/**
 * Created by admin on 13/04/2016.
 */

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private static final Lock lock = new ReentrantLock();
    public static ArrayList<ObjAppService> arrServices;
    private static ObjectMapper mapper = null;
    boolean isGrid = true;
    AlertDialog alertDialog;
    AlertDialog.Builder alertBuilder;
    Button btnPan;
    Button btnABD;
    private RecyclerViewEmptySupport mRecyclerView;
    private ProductAdapter mProductAdapter;
    private LinearLayout layoutHeader;
    private String emptyStr = "{fa-cog @dimen/dash_icon_txt_size spin}\n Loading.";
    private String noDataStr = "{fa-exclamation-triangle}\n" + "No Service Found.";


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        initView(view);
        getServices();
        loadTrendingData();
    }

    public void setGridlayout(boolean isGrid) {
        int totalCount = isGrid ? 3 : 1;
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        float yInches = metrics.heightPixels / metrics.ydpi;
        float xInches = metrics.widthPixels / metrics.xdpi;

        double diagonalInches = Math.sqrt(xInches * xInches + yInches * yInches);

        if (diagonalInches >= 6) {
            totalCount = isGrid ? 4 : 1;
        }

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), totalCount);
        final int finalTottalcount = totalCount;

        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int count = 1;
                return count;
            }
        });

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);


    }

    public void initView(View view) {

        layoutHeader = (LinearLayout) view.findViewById(R.id.layout_header);

        mRecyclerView = (RecyclerViewEmptySupport) view.findViewById(R.id.recyclerView);
        mRecyclerView.setEmptyView(view.findViewById(R.id.emptyView), emptyStr);
        mRecyclerView.getTextView().setTextColor(ContextCompat.getColor(getActivity(), R.color.color_white));
        mRecyclerView.checkIfEmpty();


        setGridlayout(isGrid);

        try {

            mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View listview, int position) {

                    if (position != -1) {

                        try {

                            ObjAppService selectedApp = arrServices.get(position);

                            if (selectedApp != null) {

                                switch (selectedApp.getAppId()) {

                                    case Constants.AboutSurat: {
                                        startActivity(new Intent(getActivity(), ActivityAboutSurat.class));
                                    }
                                    break;
                                    case Constants.AboutSMC: {
                                        startActivity(new Intent(getActivity(), ActivityAboutSMC.class));
                                    }
                                    break;
                                    case Constants.SmartCityVision: {
                                        startActivity(new Intent(getActivity(), ActivityVision.class));
                                    }
                                    break;
                                    case Constants.AboutSSCDL: {
                                        startActivity(new Intent(getActivity(), ActivitySSDL.class));
                                    }
                                    break;
                                    case Constants.Directors: {
                                        startActivity(new Intent(getActivity(), ActivityBoardOfDirectors.class));
                                    }
                                    break;
//                                    case Constants.Management: {
//                                        startActivity(new Intent(getActivity(), ActivityManagement.class));
//                                    }
//                                    break;
                                    case Constants.Project: {
//                                        CallProject();
                                        startActivity(new Intent(getActivity(), ActivityProjects.class));
                                    }
                                    break;
                                    case Constants.Tenders: {
                                        startActivity(new Intent(getActivity(), ActivityTenders.class));
                                    }
                                    break;
                                    case Constants.ContactUs: {
                                        startActivity(new Intent(getActivity(), ActivityContact.class));
                                    }
                                    break;
                                    case Constants.OurApps: {
                                        startActivity(new Intent(getActivity(), ActivityOurApps.class));
                                    }
                                    break;
                                    case  Constants.Management1:{
                                        Intent intent =  new Intent(getActivity(), DataActivity.class);
                                        startActivity(intent);
                                    }
                                    break;

                                    case Constants.BoardCommittee:{
                                        Intent intent = new Intent(getActivity(), CommiteeActivity.class);
                                        startActivity(intent);
                                    }
                                    break;

                                    case Constants.Awards:{
                                        Intent intent =  new Intent(getActivity(), AwardsActivity.class);
                                        startActivity(intent);
                                    }
                                    break;

                                    case Constants.News:{
                                        Intent intent = new Intent(getActivity(), NewsActivity.class);
                                        startActivity(intent);
                                    }
                                    break;

                                    case Constants.Videos:{
                                        Intent intent = new Intent(getActivity(), VideoActivity.class);
                                        startActivity(intent);
                                    }
                                    break;


                                    default:
                                        break;
                                }

                            }
                        } catch (Exception e) {
                            AppGlobal.showSnackBar(getView(), e.getMessage(), true);
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onItemLongClick(View view, int position) {
                }
            }));
        } catch (Exception ex) {
            Log.e("Error ", ex.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

        }

        return true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        try {

        } catch (Exception ex) {

        }
    }


    @Override
    public void onClick(View view) {
    }


    public void getServices() {

        String res = AppGlobal.loadJSONFromAsset(getActivity(), "services.json");

        try {
            ResObjAppService obj = (ResObjAppService) getMapper().readValue(res, ResObjAppService.class);

            arrServices = new ArrayList<>();
            arrServices = (ArrayList<ObjAppService>) obj.getServices();
            loadServiceData();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public void loadServiceData() {
        if (arrServices.size() > 0) {
            mProductAdapter = new ProductAdapter(getActivity(), arrServices);
            mRecyclerView.setAdapter(mProductAdapter);
        }
        mRecyclerView.checkIfEmpty();
    }

    public void loadTrendingData() {

        layoutHeader.removeAllViews();
        PromotionView promotionView = new PromotionView(getActivity());
        promotionView.viewPromotion(layoutHeader);

    }

    //Temp
    protected synchronized ObjectMapper getMapper() {

        if (mapper != null) {
            return mapper;
        }

        try {
            lock.lock();
            if (mapper == null) {

                mapper = new ObjectMapper();
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                mapper.setDateFormat(df);
                mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES,
                        false);

                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            }
            lock.unlock();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return mapper;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtName;
        private final ImageView productImage;

        public ProductViewHolder(View itemView) {
            super(itemView);

            txtName = (TextView) itemView.findViewById(R.id.dash_text);
            productImage = (ImageView) itemView.findViewById(R.id.dash_icon);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        public void bind(ObjAppService model, int position) {
            txtName.setText(model.getAppName());
            Resources resources = getActivity().getResources();
            final int resourceId = resources.getIdentifier(model.getAppIcon(), "drawable",
                    getActivity().getPackageName());

            productImage.setImageDrawable(resources.getDrawable(resourceId));
        }
    }

    public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final LayoutInflater mInflater;
        private List<ObjAppService> mModels;

        public ProductAdapter(Context context, List<ObjAppService> models) {
            mInflater = LayoutInflater.from(context);
            this.mModels = new ArrayList<>(models);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View itemView = mInflater.inflate(R.layout.layout_app_menu_item, parent, false);
            return new ProductViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final ObjAppService model = mModels.get(position);
            ((ProductViewHolder) holder).bind(model, position);
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void swap(List<ObjAppService> datas) {
            mModels.clear();
            mModels.addAll(datas);
            notifyDataSetChanged();
        }

        public void animateTo(List<ObjAppService> models) {
            applyAndAnimateRemovals(models);
            applyAndAnimateAdditions(models);
            applyAndAnimateMovedItems(models);
        }

        private void applyAndAnimateRemovals(List<ObjAppService> newModels) {

            for (int i = mModels.size() - 1; i >= 0; i--) {
                final ObjAppService model = mModels.get(i);
                if (!newModels.contains(model)) {
                    removeItem(i);
                }
            }

        }

        private void applyAndAnimateAdditions(List<ObjAppService> newModels) {

            for (int i = 0, count = newModels.size(); i < count; i++) {
                final ObjAppService model = newModels.get(i);
                if (!mModels.contains(model)) {
                    addItem(i, model);
                }
            }

        }

        private void applyAndAnimateMovedItems(List<ObjAppService> newModels) {
            for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
                final ObjAppService model = newModels.get(toPosition);
                final int fromPosition = mModels.indexOf(model);

                if (fromPosition >= 0 && fromPosition != toPosition) {
                    moveItem(fromPosition, toPosition);
                }

            }
        }

        public ObjAppService removeItem(int position) {
            final ObjAppService model = mModels.remove(position);
            notifyItemRemoved(position);
            return model;
        }

        public void addItem(int position, ObjAppService model) {
            mModels.add(position, model);
            notifyItemInserted(position);
        }

        public void moveItem(int fromPosition, int toPosition) {
            final ObjAppService model = mModels.remove(fromPosition);
            mModels.add(toPosition, model);
            notifyItemMoved(fromPosition, toPosition);
        }

    }

    private void CallProject() {

        if (btnPan == null) {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.dialog_project, null);
            btnPan = (Button) view.findViewById(R.id.btnPan);
            btnABD = (Button) view.findViewById(R.id.btnABD);
            alertBuilder = new AlertDialog.Builder(getActivity())
                    .setMessage("Please select any project.")
                    .setView(view);

            alertBuilder.setCancelable(true);

            alertDialog = alertBuilder.show();

            btnPan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), ActivityPanProjects.class));
                    alertDialog.dismiss();
                }
            });

            btnABD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getActivity(), ActivityABD.class));
                    alertDialog.dismiss();
                }
            });
        } else {
            alertDialog.show();
        }
    }

}
