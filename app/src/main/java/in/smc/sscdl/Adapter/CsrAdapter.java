package in.smc.sscdl.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.smc.sscdl.DTO.CommiteePost;
import in.smc.sscdl.R;

public class CsrAdapter extends BaseAdapter {
    List<CommiteePost.Committee.Csr> csrs;
    Context context;

    public CsrAdapter(List<CommiteePost.Committee.Csr> csrs, Context context) {
        this.csrs = csrs;
        this.context = context;
    }


    @Override
    public int getCount() {
        return csrs.size();
    }

    @Override
    public Object getItem(int position) {
        return csrs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.commitee_item,null,true);
            viewHolder.id = (TextView) convertView.findViewById(R.id.co_id);
            viewHolder.name = (TextView) convertView.findViewById(R.id.co_name);
            viewHolder.designation = (TextView) convertView.findViewById(R.id.co_designation);


            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.id.setText(csrs.get(position).getId());
        viewHolder.name.setText(csrs.get(position).getName());
        viewHolder.designation.setText(csrs.get(position).getDesignation());
        return convertView;




    }

    class ViewHolder{
        TextView id,name,designation,hrtitle;
    }
}
