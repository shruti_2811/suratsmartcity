package in.smc.sscdl.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.smc.sscdl.DTO.CommiteePost;
import in.smc.sscdl.R;

public class BmcAdapter extends BaseAdapter {
    List<CommiteePost.Committee.Pmc> pmcs;
    Context context;

    public BmcAdapter(List<CommiteePost.Committee.Pmc> pmcs, Context context) {
        this.pmcs = pmcs;
        this.context = context;
    }


    @Override
    public int getCount() {
        return pmcs.size();
    }

    @Override
    public Object getItem(int position) {
        return pmcs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.commitee_item,null,true);

            viewHolder.id_pmc = (TextView) convertView.findViewById(R.id.co_id);
            viewHolder.name_pmc = (TextView) convertView.findViewById(R.id.co_name);
            viewHolder.designation_pmc = (TextView) convertView.findViewById(R.id.co_designation);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.id_pmc.setText(pmcs.get(position).getId());
        viewHolder.name_pmc.setText(pmcs.get(position).getName());
        viewHolder.designation_pmc.setText(pmcs.get(position).getDesignation());
        return convertView;
    }
    class ViewHolder{
        TextView id_pmc,name_pmc,designation_pmc;
    }
}
