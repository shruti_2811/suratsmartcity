package in.smc.sscdl.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import in.smc.sscdl.DTO.ManagementDTO;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class AdapterOurApps extends BaseAdapter {

    private Context context;
    private ArrayList<ManagementDTO> managementDTOs ;
    private LayoutInflater inflater=null;


    public AdapterOurApps(Context context, ArrayList<ManagementDTO> managementDTOs) {
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.managementDTOs = managementDTOs;
    }

    public int getCount() {
        return managementDTOs.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_management, parent, false);
            viewHolder.imgUser = (ImageView)convertView.findViewById( R.id.img_user );
            viewHolder.tvName = (TextView)convertView.findViewById( R.id.tv_name );
            viewHolder.tvDetail = (TextView)convertView.findViewById( R.id.tv_detail );
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ManagementDTO managementDTO = managementDTOs.get(position);
        viewHolder.tvName.setText(managementDTO.getName());
        viewHolder.tvDetail.setText(managementDTO.getDesc());
        viewHolder.imgUser.setImageResource(managementDTO.getDrawable());
        // Return the completed view to render on screen
        return convertView;
    }

    private class ViewHolder{
        private ImageView imgUser;
        private TextView tvName;
        private TextView tvDetail;
    }


}
