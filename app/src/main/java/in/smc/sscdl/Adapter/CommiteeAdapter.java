package in.smc.sscdl.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.smc.sscdl.Activity.CommiteeActivity;
import in.smc.sscdl.DTO.CommiteePost;
import in.smc.sscdl.R;

public class CommiteeAdapter extends BaseAdapter {
    List<CommiteePost.Committee.Hr> hrs;

    Context context;

    public CommiteeAdapter(List<CommiteePost.Committee.Hr> hrs, Context context) {
        this.hrs = hrs;
        this.context = context;
    }



    @Override
    public int getCount() {
        return hrs.size();

    }

    @Override
    public Object getItem(int position) {
        return hrs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.commitee_item,null,true);
            viewHolder.id_hr = (TextView) convertView.findViewById(R.id.co_id);
            viewHolder.name_hr = (TextView) convertView.findViewById(R.id.co_name);
            viewHolder.designation_hr = (TextView) convertView.findViewById(R.id.co_designation);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        viewHolder.id_hr.setText(hrs.get(position).getId());
        viewHolder.name_hr.setText(hrs.get(position).getName());
        viewHolder.designation_hr.setText(hrs.get(position).getDesignation());
        return convertView;

    }
    class ViewHolder{
        TextView id_hr,name_hr,designation_hr,hrtitle;
    }
}
