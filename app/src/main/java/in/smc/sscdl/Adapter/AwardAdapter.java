package in.smc.sscdl.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.smc.sscdl.DTO.AwardResponse;
import in.smc.sscdl.Activity.FullScreenImageActivity;
import in.smc.sscdl.R;

public class AwardAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<AwardResponse.Award> awardResponseArrayList;

    public AwardAdapter(Context context, ArrayList<AwardResponse.Award> awardResponseArrayList) {
        this.context = context;
        this.awardResponseArrayList = awardResponseArrayList;
    }

    @Override
    public int getCount() {
        return awardResponseArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return awardResponseArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.awards_item,null,true);
            viewHolder.award_title = (TextView) convertView.findViewById(R.id.awardTitle);
            viewHolder.img = (ImageView)convertView.findViewById(R.id.awardPhotoThumb);
            viewHolder.award_by = (TextView) convertView.findViewById(R.id.awardBy);
            viewHolder.award_for = (TextView) convertView.findViewById(R.id.awardFor);
            viewHolder.award_date = (TextView) convertView.findViewById(R.id.awardDate);




            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Picasso.with(context.getApplicationContext()).load(awardResponseArrayList.get(position).getAwardPhotoThumb())
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(viewHolder.img);
        viewHolder.award_title.setText(awardResponseArrayList.get(position).getAwardTitle());
        viewHolder.award_for.setText(awardResponseArrayList.get(position).getAwardFor());
        viewHolder.award_by.setText(awardResponseArrayList.get(position).getAwardBy());
        viewHolder.award_date.setText(awardResponseArrayList.get(position).getAwardDate());
        Log.v("response",awardResponseArrayList.get(position).getAwardTitle());


        viewHolder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullScreenImageActivity.class);
                intent.putExtra("Image",awardResponseArrayList.get(position).getAwardPhotoBig());
                intent.putExtra("Title",awardResponseArrayList.get(position).getAwardTitle());
                intent.putExtra("award_for",awardResponseArrayList.get(position).getAwardFor());
                context.startActivity(intent);


            }
        });

        return convertView;





    }
    private class ViewHolder{
        TextView award_title,award_for,award_by,award_date;
        ImageView img;

    }
}

//public class AwardAdapter extends RecyclerView.Adapter<AwardAdapter.AwardviewHolder> {
//
//
//    List<AwardResponse.Award> awardList;
//    Context context;
//
//    public AwardAdapter(List<AwardResponse.Award> awardList, Context context) {
//        this.awardList = awardList;
//        this.context = context;
//    }
//    @NonNull
//    @NotNull
//    @Override
//    public AwardviewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.awards_item,parent,false);
//        return new AwardviewHolder(view);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull @NotNull AwardviewHolder holder, int position) {
//    AwardResponse.Award award = awardList.get(position);
//    holder.awardTitle.setText(award.getAwardTitle());
//    holder.awardfor.setText(award.getAwardFor());
//    holder.awardBy.setText(award.getAwardBy());
//    holder.awardDate.setText(award.getAwardDate());
//        Picasso.with(context.getApplicationContext()).load(awardList.get(position).getAwardPhotoThumb())
//                .placeholder(R.drawable.ic_launcher_background)
//                .fit()
//                .into(holder.awardImg);
//    }
//
//    @Override
//    public int getItemCount() {
//        return awardList.size();
//    }
//
//    public class AwardviewHolder extends RecyclerView.ViewHolder{
//
//        TextView awardTitle,awardfor,awardBy,awardDate;
//        ImageView awardImg;
//
//        public AwardviewHolder(@NonNull @NotNull View itemView) {
//            super(itemView);
//            awardTitle =(TextView) itemView.findViewById(R.id.awardTitle);
//            awardBy = (TextView) itemView.findViewById(R.id.awardBy);
//            awardfor=(TextView) itemView.findViewById(R.id.awardFor);
//            awardDate=(TextView) itemView.findViewById(R.id.awardDate);
//            awardImg= (ImageView) itemView.findViewById(R.id.awardPhotoThumb);
//        }
//    }
//}
