package in.smc.sscdl.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.smc.sscdl.DTO.Post;
import in.smc.sscdl.R;

public class PostAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Post.PostList> postLists;

    public PostAdapter(Context context, ArrayList<Post.PostList> postLists) {
        this.context = context;
        this.postLists = postLists;
    }

    @Override
    public int getCount() {
        return postLists.size();
    }

    @Override
    public Object getItem(int position) {
        return postLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.items,null,true);
            viewHolder.postname = (TextView) convertView.findViewById(R.id.postname);
            viewHolder.imgpost = (ImageView)convertView.findViewById(R.id.postimageView);
            viewHolder.postdesignation = (TextView) convertView.findViewById(R.id.posttitle);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        Picasso.with(context.getApplicationContext())
                .load(postLists.get(position).getPhoto())
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(viewHolder.imgpost);
        viewHolder.postname.setText(postLists.get(position).getName());
        viewHolder.postdesignation.setText(postLists.get(position).getDesignation());
        return convertView;
    }

    class ViewHolder{
        ImageView imgpost;
        TextView postname,postdesignation;
    }
}
