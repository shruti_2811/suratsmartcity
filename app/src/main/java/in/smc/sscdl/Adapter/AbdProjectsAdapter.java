package in.smc.sscdl.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import in.smc.sscdl.DTO.ObjAppService;
import in.smc.sscdl.R;

public class AbdProjectsAdapter extends RecyclerView.Adapter<AbdProjectsAdapter.ProductViewHolder> {

    private List<ObjAppService> mModels;
    private Context mContext;

    public AbdProjectsAdapter(Context context, List<ObjAppService> models) {
        this.mModels = new ArrayList<>(models);
        this.mContext=context;
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtName;
        private final ImageView productImage;

        public ProductViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.dash_text);
            productImage = (ImageView) itemView.findViewById(R.id.dash_icon);
        }
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_app_menu_item, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final ObjAppService model = mModels.get(position);
//        holder.bind(model,position);
        holder.txtName.setText(model.getAppName());
        Resources resources = mContext.getResources();
        final int resourceId = resources.getIdentifier(model.getAppIcon(), "drawable",
                mContext.getPackageName());

        holder.productImage.setImageDrawable(resources.getDrawable(resourceId));
        //productImage.setImageDrawable(getResources().getDrawable(R.mipmap.home));
        try {
               /* imageLoader.displayImage(WsConstants.getAppIcon(1) + model.getAppIcon(), productImage
                        , options);*/
        } catch (Exception ex) {
        }
    }
    @Override
    public int getItemCount() {
        return mModels.size();
    }

}
