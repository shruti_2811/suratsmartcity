package in.smc.sscdl.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.smc.sscdl.Activity.FullScreenImageActivity;
import in.smc.sscdl.DTO.NewsResponse;
import in.smc.sscdl.R;

public class NewsAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NewsResponse.Newslist> newslistArrayList;
    private OnclickThumbListener onclickThumbListener;

    public interface OnclickThumbListener{
        void OnclickImage(Uri imageUri);
    }

    public NewsAdapter(Context context, ArrayList<NewsResponse.Newslist> newslistArrayList) {
        this.context = context;
        this.newslistArrayList = newslistArrayList;
    }

    @Override
    public int getCount() {
        return newslistArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return newslistArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.news_item,null,true);
            viewHolder.title = (TextView) convertView.findViewById(R.id.newstitle);
            viewHolder.date  = (TextView)convertView.findViewById(R.id.newsdate);
            viewHolder.newsimg = (ImageView) convertView.findViewById(R.id.newsPhotoThumb);


            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Picasso.with(context.getApplicationContext()).load(newslistArrayList.get(position).getMediaphotothumb())
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(viewHolder.newsimg);

        viewHolder.title.setText(newslistArrayList.get(position).getMediatitle());
        viewHolder.date.setText(newslistArrayList.get(position).getMediadate());

        viewHolder.newsimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullScreenImageActivity.class);
                intent.putExtra("Image",newslistArrayList.get(position).getMediaphotobig());
                intent.putExtra("Title",newslistArrayList.get(position).getMediatitle());
                context.startActivity(intent);
            }
        });


        return convertView;
    }

    private class ViewHolder{
        TextView title,date;
        ImageView newsimg;
    }
}
