package in.smc.sscdl.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.smc.sscdl.DTO.VideoResponse;
import in.smc.sscdl.Activity.YouTubeVideo;
import in.smc.sscdl.R;

public class VideoAdapter extends BaseAdapter {
    private List<VideoResponse.Videos> videoResponseList;
    private Context context;
    Activity activity;


    public VideoAdapter(List<VideoResponse.Videos> videoResponseList, Context context) {
        this.videoResponseList = videoResponseList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return videoResponseList.size();
    }

    @Override
    public Object getItem(int position) {
        return videoResponseList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0 ;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        ViewHolder viewHolder;
        if (convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.video_item,null,true);
            viewHolder.video_title = (TextView) convertView.findViewById(R.id.iv_item_title);
            viewHolder.imagethumb = (ImageView)convertView.findViewById(R.id.iv_item_cover);

            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Picasso.with(context.getApplicationContext())
                .load(videoResponseList.get(position).getVideo_thumb_medium())
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(viewHolder.imagethumb);

        viewHolder.video_title.setText(videoResponseList.get(position).getVideo_title());

        Log.v("title",videoResponseList.get(position).getVideo_title());

        LinearLayout linearLayout = (LinearLayout) convertView.findViewById(R.id.root);
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Intent i = new Intent(context, YouTubeVideo.class);
                i.putExtra("video_embededCode",videoResponseList.get(position).getVideo_embeded());
                i.putExtra("video_title",videoResponseList.get(position).getVideo_title());
                i.putExtra("video_url",videoResponseList.get(position).getVideo_link());
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(i);

            }
        });


        return convertView;
    }
    class ViewHolder{
        ImageView imagethumb;
        TextView video_title;
    }
}
