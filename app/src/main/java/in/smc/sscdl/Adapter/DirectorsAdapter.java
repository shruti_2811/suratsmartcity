package in.smc.sscdl.Adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.smc.sscdl.Model.Directors;
import in.smc.sscdl.R;

public class DirectorsAdapter extends RecyclerView.Adapter<DirectorsAdapter.ViewHolder> {

    private ArrayList<Directors> directorsList = new ArrayList<>();
    private LayoutInflater mInflater;

    public DirectorsAdapter(Context context, ArrayList<Directors> directorsList) {
        this.mInflater = LayoutInflater.from(context);
        this.directorsList = directorsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.directorlist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.txtName.setText(getItem(position).name);
        holder.txtDesignation.setText(getItem(position).designation);
        holder.imgDirector.setImageResource(getItem(position).photo);
    }

    @Override
    public int getItemCount() {
        return directorsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtName, txtDesignation;
        public ImageView imgDirector;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtDesignation = (TextView) itemView.findViewById(R.id.txtDesignation);
            imgDirector = (ImageView) itemView.findViewById(R.id.imgDirector);
        }
    }

    public Directors getItem(int id) {
        return directorsList.get(id);
    }
}
