package in.smc.sscdl.Activity;

import android.os.Bundle;

import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;

import in.smc.sscdl.Adapter.DirectorsAdapter;
import in.smc.sscdl.Base.BaseActivity;

import in.smc.sscdl.Model.Directors;
import in.smc.sscdl.R;

public class ActivityBoardOfDirectors extends BaseActivity {

    private RecyclerView recyclerView;
    private String[] name = {"Shri M Thennarasan (IAS)",
            "Shri Shyam S Dubey", "Shri Mahendra S. Patel (IAS)", "Shri Anil Goplani",
            "Shri Chaitanya Y. Bhatt", "Shri Bharat I. Dalal", "Shri Jivan M. Patel",
            "Shri Bankim I. Desai", "Smt Kalpana A. Desai", "Shri Perci Engineer", "Shri Velji Sheta"};

    private String[] designation = {"Director & Chairman", "Director", "Director", "Director",
            "Director & CEO", "Director", "Director", "Director", "Women Independent Director",
            "Independent Director", "Independent Director"};

    private int[] photo = {R.drawable.chairman, R.drawable.shyam_dubey, R.drawable.mahendra_s_patel,
            R.drawable.anil_goplani, R.drawable.c_y_bhatt, R.drawable.b_i_dalal, R.drawable.gm_pd,
            R.drawable.gm_finance, R.drawable.kalpana_desai, R.drawable.perci_engineer,
            R.drawable.veljibhai_sheta};

    private ArrayList<Directors> directorsList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board_of_directors);
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this,
                FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Boards of Directors");

        directorsList = new ArrayList<>();
        setData();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        DirectorsAdapter adapter = new DirectorsAdapter(this, directorsList);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setData() {
        for (int i = 0; i < name.length; i++) {
            Directors d = new Directors();
            d.name = name[i];
            d.designation = designation[i];
            d.photo = photo[i];
            directorsList.add(d);
        }
    }
}
