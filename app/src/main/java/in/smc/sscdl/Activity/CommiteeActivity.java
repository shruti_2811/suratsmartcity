package in.smc.sscdl.Activity;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.smc.sscdl.Adapter.AuditAdapter;
import in.smc.sscdl.Adapter.BmcAdapter;
import in.smc.sscdl.Adapter.CommiteeAdapter;
import in.smc.sscdl.Adapter.ComplaintsAdapter;
import in.smc.sscdl.Adapter.CsrAdapter;
import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.DTO.CommiteePost;
import in.smc.sscdl.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CommiteeActivity extends BaseActivity {

    ListView lv_committee,lv_commitee_pmc,lv_commitee_audit,lv_commitee_csr,lv_commitee_complaits;
    TextView txthr,txtpmc,txtaudit,txtcsr,txtcomplaints;

    CommiteeAdapter commiteeAdapter;
    List<CommiteePost.Committee.Hr> hrs;
    ImageButton hrbtn,pmcbtn,auditbtn,csrbtn,complaintsbtn;
    List<CommiteePost.Committee.Pmc> pmcs;
    List<CommiteePost.Committee.Audit> audits;
    LinearLayout hr_layout;
    List<CommiteePost.Committee.Csr> csrs;
    List<CommiteePost.Committee.Complaint> complaints;
    CardView fixed_layout,fix_layout_pmc,fix_layout_audit,fix_layout_csr,fix_layout_complaints;
    LinearLayout hidden_layout,hidden_layout_pmc,hidden_layout_audit,hidden_layout_csr,hidden_layout_complaitns;

    HashMap<String, List<CommiteeAdapter>> expandableDetaillist = new HashMap<String, List<CommiteeAdapter>>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commitee);

        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Board of Committee");


        txthr = findViewById(R.id.txtHR);
        fixed_layout = findViewById(R.id.fixed_layout);
        hidden_layout = findViewById(R.id.hidden_layout);
        lv_committee = (ListView) findViewById(R.id.lv_commitee);
        hrbtn = (ImageButton) findViewById(R.id.hrbtn);
        hr_layout = findViewById(R.id.hr_layout);
        hrbtn.setBackgroundDrawable(null);


        txtpmc = findViewById(R.id.txtpmc);
        fix_layout_pmc = findViewById(R.id.fixed_layout_pmc);
        hidden_layout_pmc = findViewById(R.id.hidden_layout_pmc);
        lv_commitee_pmc = (ListView) findViewById(R.id.lv_commitee_pmc);
        pmcbtn = findViewById(R.id.pmcbtn);
        pmcbtn.setBackgroundDrawable(null);

        txtaudit = findViewById(R.id.txtaudit);
        fix_layout_audit = findViewById(R.id.fixed_layout_audit);
        hidden_layout_audit = findViewById(R.id.hidden_layout_audit);
        lv_commitee_audit = (ListView) findViewById(R.id.lv_commitee_audit);
        auditbtn = findViewById(R.id.auditbtn);
        auditbtn.setBackgroundDrawable(null);

        txtcsr = findViewById(R.id.txtcsr);
        fix_layout_csr = findViewById(R.id.fixed_layout_csr);
        hidden_layout_csr = findViewById(R.id.hidden_layout_csr);
        lv_commitee_csr = (ListView) findViewById(R.id.lv_commitee_csr);
        csrbtn = findViewById(R.id.csrbtn);
        csrbtn.setBackgroundDrawable(null);

        txtcomplaints = findViewById(R.id.txtcomplaints);
        fix_layout_complaints = findViewById(R.id.fixed_layout_complaints);
        hidden_layout_complaitns = findViewById(R.id.hidden_layout_complaints);
        lv_commitee_complaits = (ListView) findViewById(R.id.lv_commitee_complaits);
        complaintsbtn  = findViewById(R.id.complaintsbtn);
        complaintsbtn.setBackgroundDrawable(null);










                Gson gson = new GsonBuilder()
                        .setLenient()
                        .create();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(JsonPlaceHolder.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                JsonPlaceHolder jsoNplaceholder = retrofit.create(JsonPlaceHolder.class);

                Call<CommiteePost> call = jsoNplaceholder.getCommitee("sscdlandroid", "8e9f1685d05588f41a49ddb5b4bef36a");
                call.enqueue(new Callback<CommiteePost>() {
                    @Override
                    public void onResponse(Call<CommiteePost> call, Response<CommiteePost> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(CommiteeActivity.this, response.code(), Toast.LENGTH_LONG).show();
                            return;
                        }

                        CommiteePost resource = response.body();
                        assert resource != null;
                        hrs = resource.committee.hr;
                         commiteeAdapter = new CommiteeAdapter(hrs, CommiteeActivity.this);
                        lv_committee.setAdapter(commiteeAdapter);
                        List<CommiteeAdapter> hr_committe = new ArrayList<>();
                        hr_committe.add(commiteeAdapter);
                        expandableDetaillist.put("HR Committee",hr_committe);
                    }
                    @Override
                    public void onFailure(Call<CommiteePost> call, Throwable t) {
                        Toast.makeText(CommiteeActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


        hrbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hrbtn.setRotation(180);

                if (hidden_layout.getVisibility()==View.VISIBLE){
                    TransitionManager.beginDelayedTransition(fixed_layout,new AutoTransition());
                    hidden_layout.setVisibility(View.GONE);
                    hrbtn.setRotation(0);




                }
                else {
                    TransitionManager.beginDelayedTransition(fixed_layout,new AutoTransition());

                    hidden_layout.setVisibility(View.VISIBLE);
                    hidden_layout_pmc.setVisibility(View.GONE);
                    hidden_layout_csr.setVisibility(View.GONE);
                    hidden_layout_complaitns.setVisibility(View.GONE);
                    hidden_layout_audit.setVisibility(View.GONE);


                }

            }


        });

        Gson gson1 = new  GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson1))
                .build();

        JsonPlaceHolder jsoNplaceholder1 = retrofit1.create(JsonPlaceHolder.class);
        Call<CommiteePost> call1 = jsoNplaceholder1.getCommitee("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call1.enqueue(new Callback<CommiteePost>() {
            @Override
            public void onResponse(Call<CommiteePost> call, Response<CommiteePost> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(CommiteeActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                CommiteePost resource = response.body();
                assert  resource != null;
                pmcs = resource.committee.pmc;
                BmcAdapter bmcAdapter = new BmcAdapter(pmcs,CommiteeActivity.this);
                lv_commitee_pmc.setAdapter(bmcAdapter);
            }
            @Override
            public void onFailure(Call<CommiteePost> call, Throwable t) {
                Toast.makeText(CommiteeActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        pmcbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pmcbtn.setRotation(180);

                if (hidden_layout_pmc.getVisibility()==View.VISIBLE){
                    TransitionManager.beginDelayedTransition(fix_layout_pmc,new AutoTransition());
                    hidden_layout_pmc.setVisibility(View.GONE);
                    pmcbtn.setRotation(0);
                }
                else {
                    TransitionManager.beginDelayedTransition(fix_layout_pmc,new AutoTransition());
                    hidden_layout_pmc.setVisibility(View.VISIBLE);
                    hidden_layout.setVisibility(View.GONE);
                    hidden_layout_csr.setVisibility(View.GONE);
                    hidden_layout_complaitns.setVisibility(View.GONE);
                    hidden_layout_audit.setVisibility(View.GONE);
                }


            }
        });

        Gson gson2 = new  GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson2))
                .build();

        JsonPlaceHolder jsoNplaceholder2 = retrofit2.create(JsonPlaceHolder.class);
        Call<CommiteePost> call2 = jsoNplaceholder2.getCommitee("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call2.enqueue(new Callback<CommiteePost>() {
            @Override
            public void onResponse(Call<CommiteePost> call, Response<CommiteePost> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(CommiteeActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                CommiteePost resource = response.body();
                assert  resource != null;
                audits = resource.committee.audit;
                AuditAdapter auditAdapter = new AuditAdapter(audits,CommiteeActivity.this);
                lv_commitee_audit.setAdapter(auditAdapter);
            }

            @Override
            public void onFailure(Call<CommiteePost> call, Throwable t) {

            }
        });


        auditbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auditbtn.setRotation(180);

                if (hidden_layout_audit.getVisibility()==View.VISIBLE){
                    TransitionManager.beginDelayedTransition(fix_layout_audit,new AutoTransition());
                    hidden_layout_audit.setVisibility(View.GONE);
                    auditbtn.setRotation(0);
                }
                else {
                    TransitionManager.beginDelayedTransition(fix_layout_audit,new AutoTransition());
                    hidden_layout_audit.setVisibility(View.VISIBLE);
                    hidden_layout.setVisibility(View.GONE);
                    hidden_layout_pmc.setVisibility(View.GONE);
                    hidden_layout_csr.setVisibility(View.GONE);
                    hidden_layout_complaitns.setVisibility(View.GONE);

                }



            }
        });

        Gson gson3 = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit3 = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson3))
                .build();

        JsonPlaceHolder jsoNplaceholder3 = retrofit3.create(JsonPlaceHolder.class);
        Call<CommiteePost> call3 = jsoNplaceholder3.getCommitee("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call3.enqueue(new Callback<CommiteePost>() {
            @Override
            public void onResponse(Call<CommiteePost> call, Response<CommiteePost> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(CommiteeActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                CommiteePost resource = response.body();
                assert resource != null;
                csrs = resource.committee.csr;
                CsrAdapter csrAdapter = new CsrAdapter(csrs,CommiteeActivity.this);
                lv_commitee_csr.setAdapter(csrAdapter);
            }
            @Override
            public void onFailure(Call<CommiteePost> call, Throwable t) {
                Toast.makeText(CommiteeActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        csrbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                csrbtn.setRotation(180);

                if (hidden_layout_csr.getVisibility()==View.VISIBLE){
                    TransitionManager.beginDelayedTransition(fix_layout_csr,new AutoTransition());
                    hidden_layout_csr.setVisibility(View.GONE);
                    csrbtn.setRotation(0);
                }
                else {
                    TransitionManager.beginDelayedTransition(fix_layout_csr,new AutoTransition());
                    hidden_layout_csr.setVisibility(View.VISIBLE);
                    hidden_layout.setVisibility(View.GONE);
                    hidden_layout_pmc.setVisibility(View.GONE);
                    hidden_layout_complaitns.setVisibility(View.GONE);
                    hidden_layout_audit.setVisibility(View.GONE);
                }
            }
        });


        Gson gson4 = new  GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit4 = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson4))
                .build();

        JsonPlaceHolder jsoNplaceholder4 = retrofit4.create(JsonPlaceHolder.class);
        Call<CommiteePost> call4 = jsoNplaceholder4.getCommitee("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call4.enqueue(new Callback<CommiteePost>() {
            @Override
            public void onResponse(Call<CommiteePost> call, Response<CommiteePost> response) {
                if(!response.isSuccessful()){
                    Toast.makeText(CommiteeActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }

                CommiteePost resource = response.body();
                assert  resource != null;
                complaints = resource.committee.complaint;
                ComplaintsAdapter complaintsAdapter = new ComplaintsAdapter(complaints,CommiteeActivity.this);
                lv_commitee_complaits.setAdapter(complaintsAdapter);
            }

            @Override
            public void onFailure(Call<CommiteePost> call, Throwable t) {
                Toast.makeText(CommiteeActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        complaintsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                complaintsbtn.setRotation(180);

                if (hidden_layout_complaitns.getVisibility()==View.VISIBLE){
                    TransitionManager.beginDelayedTransition(fix_layout_complaints,new AutoTransition());
                    hidden_layout_complaitns.setVisibility(View.GONE);
                    complaintsbtn.setRotation(0);
                }
                else {
                    TransitionManager.beginDelayedTransition(fix_layout_complaints,new AutoTransition());
                    hidden_layout_complaitns.setVisibility(View.VISIBLE);
                    hidden_layout.setVisibility(View.GONE);
                    hidden_layout_pmc.setVisibility(View.GONE);
                    hidden_layout_csr.setVisibility(View.GONE);
                    hidden_layout_audit.setVisibility(View.GONE);
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}