package in.smc.sscdl.Activity;

import androidx.annotation.NonNull;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;

import in.smc.sscdl.Adapter.PostAdapter;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.DTO.Post;
import in.smc.sscdl.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataActivity extends BaseActivity {

    ListView iv_post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Management");

        iv_post = (ListView) findViewById(R.id.iv_post);

        ProgressDialog mProgressDialog = new ProgressDialog(DataActivity.this);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setTitle("Management");
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        mProgressDialog.show();



        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        JsonPlaceHolder jsoNplaceholder = retrofit.create(JsonPlaceHolder.class);
        Call<Post> call = jsoNplaceholder.getpost("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(DataActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                mProgressDialog.dismiss();
                Post resource =  response.body();
                assert resource != null;
                ArrayList<Post.PostList> postList = (ArrayList<Post.PostList>) resource.management;
                PostAdapter postAdapter = new PostAdapter(DataActivity.this,  postList);
                iv_post.setAdapter(postAdapter);
                postAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.wtf("log",t.getMessage());
                Toast.makeText(DataActivity.this, t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
