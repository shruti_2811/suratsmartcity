package in.smc.sscdl.Activity;

import android.content.Intent;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityProjects extends BaseActivity implements View.OnClickListener {

    //    private WebView wvView;
    private LinearLayout ll_abdProject, ll_PanProject;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        findViews();
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Projects");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void findViews() {

        ll_abdProject = (LinearLayout) findViewById(R.id.ll_abdProject);
        ll_PanProject = (LinearLayout) findViewById(R.id.ll_PanProject);

        ll_abdProject.setOnClickListener(this);
        ll_PanProject.setOnClickListener(this);
    }

    private void populate() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_abdProject:
                startActivity(new Intent(ActivityProjects.this, ActivityABDProjects.class));
                break;
            case R.id.ll_PanProject:
                startActivity(new Intent(ActivityProjects.this, ActivityPanProjects.class));
                break;
        }
    }
}
