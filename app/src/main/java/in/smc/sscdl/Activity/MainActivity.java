package in.smc.sscdl.Activity;

import android.os.Bundle;
import android.os.Handler;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.Base.BaseFragment;
import in.smc.sscdl.Fragment.HomeFragment;
import in.smc.sscdl.R;

public class MainActivity extends BaseActivity {


    private static long back_pressed;
    public RelativeLayout fragmentContainer;
    View header;
    Menu navMenu;


    private FragmentManager.OnBackStackChangedListener
            mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            syncActionBarArrowState();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentContainer = (RelativeLayout) findViewById(R.id.fragmentContainer);
        initView();

        initToolbar();
        toolBarWithDrawableIconTitle(ContextCompat.getDrawable(this, R.drawable.home), getResources().getString(R.string.app_name));
    }

    private void initView() {

        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
        BaseFragment.replaceFragmentInContainer(new HomeFragment(), fragmentContainer.getId(), getSupportFragmentManager());

    }

    private void syncActionBarArrowState() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                final ActionBar actionBar = getSupportActionBar();
                if (actionBar != null) {
                    if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                actionBar.setHomeButtonEnabled(false);
                                actionBar.setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_material);
                                actionBar.setDisplayHomeAsUpEnabled(true);


                            }
                        });
                    } else if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
                        actionBar.setDisplayHomeAsUpEnabled(true);
                        actionBar.setHomeButtonEnabled(true);
                        actionBar.setHomeAsUpIndicator(new IconDrawable(MainActivity.this, FontAwesomeIcons.fa_bars)
                                .colorRes(R.color.icons)
                                .actionBarSize());
                    }
                }
            }
        });

    }

}
