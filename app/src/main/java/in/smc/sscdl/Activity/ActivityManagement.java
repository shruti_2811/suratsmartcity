package in.smc.sscdl.Activity;

import android.os.Bundle;

import android.view.MenuItem;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;

import in.smc.sscdl.Adapter.AdapterManagement;
import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.DTO.ManagementDTO;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityManagement extends BaseActivity {


    private ListView lvMgmnt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management);
        init();
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Management");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        lvMgmnt = (ListView) findViewById(R.id.lv_mgmnt);
        populate();

    }

    private void populate() {
        ArrayList<ManagementDTO> managementDTOs = new ArrayList<>();

        managementDTOs.add(new ManagementDTO(getString(R.string.chairman_name), getString(R.string.chairman), R.drawable.chairman));
        managementDTOs.add(new ManagementDTO(getString(R.string.gm_name), getString(R.string.gm_detail), R.drawable.c_y_bhatt));
        managementDTOs.add(new ManagementDTO(getString(R.string.dalal), getString(R.string.dalal_detail), R.drawable.b_i_dalal));
        managementDTOs.add(new ManagementDTO(getString(R.string.gm_pd_name), getString(R.string.gm_pd_detail), R.drawable.gm_pd));
        managementDTOs.add(new ManagementDTO(getString(R.string.patel), getString(R.string.patel_detail), R.drawable.patel));
        managementDTOs.add(new ManagementDTO(getString(R.string.gm_finance_name), getString(R.string.gm_finance_detail), R.drawable.gm_finance));
        managementDTOs.add(new ManagementDTO(getString(R.string.r_j_pandaya), getString(R.string.r_j_pandaya_detail), R.drawable.r_j_pandya));
        managementDTOs.add(new ManagementDTO(getString(R.string.khatvani), getString(R.string.khatvani_detail), R.drawable.katwani));
        managementDTOs.add(new ManagementDTO(getString(R.string.j_m_desail), getString(R.string.j_m_desail_detail), R.drawable.j_m_desai));
        managementDTOs.add(new ManagementDTO(getString(R.string.a_m_dube), getString(R.string.a_m_dube_detail), R.drawable.a_m_dube));
        managementDTOs.add(new ManagementDTO(getString(R.string.j_j_patel), getString(R.string.j_j_patel_detail), R.drawable.jjpatel));
        managementDTOs.add(new ManagementDTO(getString(R.string.ravin_sanghavi), getString(R.string.ravin_sanghavi_detail), R.drawable.ravin_sanghvi));

        lvMgmnt.setAdapter(new AdapterManagement(this, managementDTOs, ""));
    }
}
