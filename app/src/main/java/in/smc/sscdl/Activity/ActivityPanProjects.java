package in.smc.sscdl.Activity;

import android.os.Bundle;

import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityPanProjects extends BaseActivity {

    private WebView wvView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan);
        findViews();
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Pan City Projects");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void findViews() {
        wvView = (WebView)findViewById( R.id.wvView );
        populate();
    }

    private void populate(){
        wvView.loadUrl("file:///android_asset/pan.html");

    }
}
