package in.smc.sscdl.Activity;

import android.app.ProgressDialog;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityTenders  extends BaseActivity {
  private WebView wvView;
  ProgressDialog mProgressDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_tenders);
    findViews();
    initToolbar();
    toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Tenders");
    mProgressDialog = new ProgressDialog(ActivityTenders.this);
    mProgressDialog.setCancelable(true);
    mProgressDialog.setTitle("Tenders");
    mProgressDialog.setMessage("Please Wait...");
    mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    mProgressDialog.show();


  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;

    }

    return super.onOptionsItemSelected(item);
  }
  private void findViews() {
    wvView = (WebView)findViewById( R.id.wvView );
    populate();
  }

  private void populate(){


    wvView.setWebChromeClient(new WebChromeClient());
    wvView.setWebViewClient( new MyWebChromeClient());
//    wvView.setWebViewClient( new webClient());
    wvView.getSettings().setJavaScriptEnabled(true);
    wvView.getSettings().setSupportZoom(true);
    wvView.setHorizontalScrollBarEnabled(true);
    wvView.getSettings().setLoadWithOverviewMode(true);


    wvView.loadUrl("http://www.suratsmartcity.com/Tenders/Tenders");
  }

//  public class webClient extends WebViewClient {
//    public void onProgressChanged(WebView view, int newProgress) {
//      if (mProgressDialog != null){
//        mProgressDialog.dismiss();
//      }
//    }
//  }


  public class MyWebChromeClient extends WebViewClient {
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
      mProgressDialog.dismiss();
      view.loadUrl(url);
      return true;
    }

//    @Override
//    public void onPageFinished(WebView view, String url) {
//      super.onPageFinished(view, url);
//      mProgressDialog.dismiss();
//    }

  }





}
