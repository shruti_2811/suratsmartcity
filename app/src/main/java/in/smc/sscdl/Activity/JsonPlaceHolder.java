package in.smc.sscdl.Activity;



import in.smc.sscdl.DTO.AwardResponse;
import in.smc.sscdl.DTO.CommiteePost;
import in.smc.sscdl.DTO.NewsResponse;
import in.smc.sscdl.DTO.Post;
import in.smc.sscdl.DTO.VideoResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface JsonPlaceHolder {
    String BASE_URL = "http://www.sscdl.in/m-app/";

    @GET("management.php")
    Call<Post> getpost(
            @Query("appuser") String appuser,
            @Query("appkey") String appkey
    );

    @GET("committees.php")
    Call<CommiteePost> getCommitee(
            @Query("appuser") String appuser,
            @Query("appkey") String appkey
    );

    @GET("awards.php")
    Call<AwardResponse> getAwards(
            @Query("appuser") String appuser,
            @Query("appkey") String appkey
    );

    @GET("media-coverage.php")
    Call<NewsResponse> getNews(
            @Query("appuser") String appuser,
            @Query("appkey") String appkey
    );

    @GET("video-gallery.php")
    Call<VideoResponse> getVideos(
            @Query("appuser") String appuser,
            @Query("appkey") String appkey
    );
}
