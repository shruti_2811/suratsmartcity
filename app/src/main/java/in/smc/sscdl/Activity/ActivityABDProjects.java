package in.smc.sscdl.Activity;

import android.content.Context;
import android.os.Bundle;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import in.smc.sscdl.Adapter.AbdProjectsAdapter;
import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.Constants.AppGlobal;
import in.smc.sscdl.DTO.ObjAppService;
import in.smc.sscdl.DTO.ResObjAppProjectService;
import in.smc.sscdl.DTO.ResObjAppService;
import in.smc.sscdl.Fragment.HomeFragment;
import in.smc.sscdl.R;
import in.smc.sscdl.Utils.AutoScrollViewPager;
import in.smc.sscdl.Utils.CirclePageIndicator;
import in.smc.sscdl.Utils.RecyclerViewSupport.RecyclerViewEmptySupport;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityABDProjects extends BaseActivity {

    private WebView wvView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan);
        findViews();
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this,
                FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Area Based Development Projects");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void findViews() {
        wvView = (WebView) findViewById(R.id.wvView);
        populate();
    }

    private void populate() {
        wvView.loadUrl("file:///android_asset/abd.html");
    }
}