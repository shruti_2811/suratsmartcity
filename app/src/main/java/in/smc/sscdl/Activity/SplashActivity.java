package in.smc.sscdl.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.multidex.BuildConfig;

import java.util.ArrayList;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.Base.PermissionManager;
import in.smc.sscdl.R;

/**
 * Created by admin on 22/03/2017.
 */

public class SplashActivity extends BaseActivity implements PermissionManager.FullCallback {

    private static int SPLASH_TIME_OUT = 3000;
    private String TAG = SplashActivity.class.getSimpleName();


    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);


        checkDevicePermission();

    }


    /* -- Permission Manager Callback -- */
    public void checkDevicePermission() {
        try {
            ArrayList<PermissionManager.PermissionEnum> permissionsenum = new ArrayList<>();
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_PERMISSIONS);
            if (info.requestedPermissions != null) {
                for (String p : info.requestedPermissions) {
                    PermissionManager.PermissionEnum enumvalue = PermissionManager.PermissionEnum.fromManifestPermission(p);
                    if (enumvalue != null) {
                        permissionsenum.add(PermissionManager.PermissionEnum.fromManifestPermission(p));
                    }
                }
            }
            PermissionManager.with(SplashActivity.this)
                    .permissions(permissionsenum)
                    .callback(SplashActivity.this)
                    .askagain(true)
                    .askagainCallback(new PermissionManager.AskagainCallback() {
                        @Override
                        public void showRequestPermission(UserResponse response) {
                            showDialog(response);
                        }
                    })
                    .ask();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handleResult(requestCode, permissions, grantResults);
    }


    @Override
    public void result(ArrayList<PermissionManager.PermissionEnum> permissionsGranted,
                       ArrayList<PermissionManager.PermissionEnum> permissionsDenied,
                       ArrayList<PermissionManager.PermissionEnum> permissionsDeniedForever,
                       ArrayList<PermissionManager.PermissionEnum> permissionsAsked) {

        if (permissionsDenied.size() > 0 || permissionsDeniedForever.size() > 0) {

            displayPermissionAlert();

        } else {
            navigateToMainScreen();
        }


    }

    public void navigateToMainScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }, SPLASH_TIME_OUT);
    }


    public void displayPermissionAlert() {
        new AlertDialog.Builder(SplashActivity.this)
                .setTitle("Permission Alert !")
                .setMessage("You have not allowed enough permission to run this application. press Ok to allow permission and close application to run.")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        PermissionManager.PermissionUtils.openApplicationSettings(SplashActivity.this, BuildConfig.APPLICATION_ID);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void showDialog(final PermissionManager.AskagainCallback.UserResponse response) {
        new AlertDialog.Builder(SplashActivity.this)
                .setTitle("Permission needed")
                .setMessage("This app realy need to use permissions, do you want to authorize it?")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        response.result(true);
                    }
                })
                .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        response.result(false);
                        finish();
                    }
                })
                .show();
    }

    private void showReqPermissionDialog() {
        new AlertDialog.Builder(SplashActivity.this)
                .setTitle("Permission needed")
                .setMessage("This app realy need to use permissions, do you want to authorize it?")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkDevicePermission();
                    }
                })
                .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

}
