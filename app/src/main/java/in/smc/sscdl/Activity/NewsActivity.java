package in.smc.sscdl.Activity;

import androidx.annotation.NonNull;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;
import java.util.List;

import in.smc.sscdl.Adapter.NewsAdapter;
import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.DTO.NewsResponse;
import in.smc.sscdl.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NewsActivity extends BaseActivity {

    ListView lv_news;
    ImageView bigImg;
    List<NewsResponse.Newslist> newslists1;
    private ProgressDialog mprocessingdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        mprocessingdialog = new ProgressDialog(this);

        ProgressDialog mProgressDialog = new ProgressDialog(NewsActivity.this);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setTitle("News");
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();



        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "News");

        lv_news = (ListView) findViewById(R.id.news_lv);
        bigImg = (ImageView) findViewById(R.id.FullScreenimageView);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(JsonPlaceHolder.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        JsonPlaceHolder jsoNplaceholder =  retrofit.create(JsonPlaceHolder.class);
        Call<NewsResponse> call = jsoNplaceholder.getNews("sscdlandroid","8e9f1685d05588f41a49ddb5b4bef36a");
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                if ((!response.isSuccessful())){
                    Toast.makeText(NewsActivity.this,response.code(), Toast.LENGTH_LONG).show();
                    return;
                }
                mProgressDialog.dismiss();
                NewsResponse resource = (NewsResponse) response.body();
                newslists1 = resource.mediacoverage;
                NewsAdapter newsAdapter = new NewsAdapter(NewsActivity.this, (ArrayList<NewsResponse.Newslist>) newslists1);
                lv_news.setAdapter( newsAdapter);
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                Toast.makeText(NewsActivity.this, t.getMessage() , Toast.LENGTH_LONG).show();
            }
        });

        ImageView imageThumb = findViewById(R.id.newsPhotoThumb);





    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
