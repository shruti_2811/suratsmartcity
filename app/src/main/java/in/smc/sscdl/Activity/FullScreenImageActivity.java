package in.smc.sscdl.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.github.chrisbanes.photoview.PhotoView;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.ortiz.touchview.TouchImageView;
import com.squareup.picasso.Picasso;
import com.zolad.zoominimageview.ZoomInImageView;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.R;

public class FullScreenImageActivity extends BaseActivity {
    private PhotoView imageView;
    ImageButton close_icon;
    ProgressDialog mProgressDialog;
    TextView title_fl;
    String title,award_for;
    String image;
    private ScaleGestureDetector scaleGestureDetector;
    private float mScaleFactor = 1.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

         image = getIntent().getStringExtra("Image");
         title = getIntent().getStringExtra("Title");
         award_for = getIntent().getStringExtra("award_for");

        imageView =  findViewById(R.id.FullScreenimageView);

        title_fl = findViewById(R.id.fullscreen_title);

        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_close), R.color.colorAccent, title);

        mProgressDialog = new ProgressDialog(FullScreenImageActivity.this);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();

        getdata();



    }

    private void getdata() {
        if (mProgressDialog != null){
            mProgressDialog.dismiss();
        }
        Picasso.with(this.getApplicationContext()).load(image)
                .placeholder(R.drawable.ic_launcher_background)
                .fit()
                .into(imageView);

        title_fl.setText(award_for);

//        close_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                imageView.setVisibility(View.GONE);
//                finish();
//
//            }
//        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
//        return true;
//    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                this.finish();




                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
//    @Override
//    public boolean onTouchEvent(MotionEvent motionEvent) {
//        scaleGestureDetector.onTouchEvent(motionEvent);
//        return true;
//    }
//    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
//        @Override
//        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
//            mScaleFactor *= scaleGestureDetector.getScaleFactor();
//            mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 10.0f));
//            imageView.setScaleX(mScaleFactor);
//            imageView.setScaleY(mScaleFactor);
//            return true;
//        }
//    }
}