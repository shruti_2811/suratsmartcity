package in.smc.sscdl.Activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;

import in.smc.sscdl.Adapter.AdapterManagement;
import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.DTO.ManagementDTO;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityOurApps extends BaseActivity {

  private ListView lvMgmnt;
  private int PLAY_SERVICES_RESOLUTION_REQUEST = 100;
  ArrayList<ManagementDTO> managementDTOs = new ArrayList<>();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_management);
    init();
    initToolbar();
    toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Our Apps");
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;

    }

    return super.onOptionsItemSelected(item);
  }

  private void init() {
    lvMgmnt = (ListView) findViewById(R.id.lv_mgmnt);
    lvMgmnt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        try {
          ManagementDTO managementDTO = managementDTOs.get(i);
          if (isPackageInstalled(managementDTO.getApppackage())) {
            Intent launchIntent = getPackageManager().getLaunchIntentForPackage(managementDTO.getApppackage());
            if (launchIntent != null) {
              startActivity(launchIntent);//null pointer check in case package name was not found
            }
          } else {
            if (checkPlayServices()) {
              Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(managementDTO.getUrl()));
              startActivity(intent);
            }
          }
        } catch (ActivityNotFoundException e) {
          e.printStackTrace();
        }
      }
    });
    populate();

  }

  private void populate() {


    managementDTOs.add(new ManagementDTO(getString(R.string.smcapp), getString(R.string.smcapp_detail), R.drawable.smcapp, "market://details?id=in.smc", "in.smc"));
    managementDTOs.add(new ManagementDTO(getString(R.string.heritage), getString(R.string.heritage_detail), R.drawable.heritage, "market://details?id=in.smc.suratheritagewalk", "in.smc.suratheritagewalk"));
    managementDTOs.add(new ManagementDTO(getString(R.string.sitilink), getString(R.string.sitilink_detail), R.drawable.sitilink_logo, "market://details?id=in.sscdl.suratsitilink", "in.sscdl.suratsitilink"));

    lvMgmnt.setAdapter(new AdapterManagement(this, managementDTOs, "our"));
  }

  private boolean isPackageInstalled(String packagename) {
    try {

      getPackageManager().getPackageInfo(packagename, 0);
      return true;
    } catch (PackageManager.NameNotFoundException e) {
      return false;
    }
  }

  private boolean checkPlayServices() {
    GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
    int resultCode = googleAPI
            .isGooglePlayServicesAvailable(this);
    if (resultCode != ConnectionResult.SUCCESS) {
      if (googleAPI.isUserResolvableError(resultCode)) {
        googleAPI.getErrorDialog(this, resultCode,
                PLAY_SERVICES_RESOLUTION_REQUEST).show();
      } else {
        Toast.makeText(this,
                "Play service not available in your device", Toast.LENGTH_LONG)
                .show();
      }
      return false;
    }
    return true;
  }
}
