package in.smc.sscdl.Activity;

import android.os.Bundle;

import android.view.MenuItem;

import androidx.annotation.Nullable;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import in.smc.sscdl.Base.BaseActivity;
import in.smc.sscdl.R;

/**
 * Created by Administrator on 01/07/2017.
 */

public class ActivityVision extends BaseActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vision);
        initToolbar();
        toolBarWithIconTitle(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left), R.color.colorAccent, "Smart City Vision");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
