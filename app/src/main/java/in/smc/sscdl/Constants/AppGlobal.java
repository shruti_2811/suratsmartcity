package in.smc.sscdl.Constants;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.io.InputStream;

import in.smc.sscdl.R;


/**
 * Created by Administrator on 14/04/2016.
 */
public class AppGlobal {

  public static final int NO_VALUE = 0;
  public static final String NO_VALUE_PREF = "";
  public static final String KEY_LANGUAGE = "KEY_LANGUAGE";
  public static AlertDialog alertDialog;
  private static ProgressDialog progressDialog;

  public static boolean isValidEmail(CharSequence target) {
    if (target == null) {
      return false;
    } else {
      return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
  }

  public static void showSnackBar(View container, String message, boolean isLong) {

    Snackbar snack = Snackbar.make(container, message, isLong ? Snackbar.LENGTH_LONG : Snackbar.LENGTH_SHORT);
    View view = snack.getView();
    TextView tv = (TextView) view.findViewById(R.id.snackbar_text);
    tv.setTextColor(Color.WHITE);
    snack.show();

  }

  /**
   * Display Toast
   *
   * @param context
   * @param message
   */
  public static void showToast(Context context, String message) {
    if (context != null)
      Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
  }

  /**
   * Show Progress Dialog
   *
   * @param context
   * @param msg
   */
  public static void showProgressDialogAlert(Context context, String msg) {
    progressDialog = new ProgressDialog(context);
    progressDialog.setMessage(msg);
    progressDialog.setCanceledOnTouchOutside(false);
    progressDialog.setCancelable(false);
    progressDialog.show();
  }

  /**
   * Hide Progress Dialog
   *
   * @param context
   */
  public static void hideProgressDialogAlert(Context context) {
    if (progressDialog != null) {
      progressDialog.dismiss();
    }
  }


  /**
   * check Network Connection
   *
   * @param context
   * @return
   */
  public static boolean isNetworkAvailable(Context context) {

    ConnectivityManager cm = (ConnectivityManager) context
            .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
      return true;
    }
    return false;
  }


  /**
   * Hide Keyboard
   *
   * @param mContext
   */
  public static void hideKeyboard(Context mContext) {

    try {
      InputMethodManager imm = (InputMethodManager) mContext
              .getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(((Activity) mContext).getWindow()
              .getCurrentFocus().getWindowToken(), 0);
    } catch (Exception e) {
      e.printStackTrace();
    }


  }


  /**
   * Load json From Assets
   *
   * @param mContext
   * @param fileName
   * @return
   */
  public static String loadJSONFromAsset(Context mContext, String fileName) {
    String json = null;
    try {
      InputStream is = mContext.getAssets().open(fileName);
      int size = is.available();
      byte[] buffer = new byte[size];
      is.read(buffer);
      is.close();
      json = new String(buffer, "UTF-8");
    } catch (IOException ex) {
      ex.printStackTrace();
      return null;
    }
    return json;
  }

  /**
   * add string preference in private shared
   *
   * @param context -
   * @param key
   * @param value
   */
  public static void setStringPref(Context context, String key, String value) {
    context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE).edit().putString(key, value).commit();
  }

  /**
   * @param context
   * @param key
   * @return
   */
  public static String getStringPref(Context context, String key) {
    return context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE).getString(key, NO_VALUE_PREF);
  }

  /**
   * add string preference in private shared
   *
   * @param context -
   * @param key
   * @param value
   */
  public static void setIntegerPref(Context context, String key, int value) {
    context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE).edit().putInt(key, value).commit();
  }

  /**
   * @param context
   * @param key
   * @return
   */
  public static int getIntegerPref(Context context, String key) {
    return context.getSharedPreferences(Constants.PREFERENCE_NAME, Context.MODE_PRIVATE).getInt(key, NO_VALUE);
  }


  public static void displayAlertDialog(Context mContext, String title, String msg) {

    if (alertDialog != null && alertDialog.isShowing())
      alertDialog.dismiss();

    AlertDialog.Builder builder = new AlertDialog.Builder(mContext)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int which) {
                // continue
              }
            })
            .setIcon(android.R.drawable.ic_dialog_alert);

    alertDialog = builder.create();
    alertDialog.show();
  }


  public enum DEVICEID {
    ANDROID(2),
    IOS(3),
    WINDOWS(4);


    private String stringValue;
    private int intValue;

    private DEVICEID(String toString, int value) {
      stringValue = toString;
      intValue = value;
    }

    private DEVICEID(int value) {
      intValue = value;
    }

    @Override
    public String toString() {
      return stringValue;
    }

    public int toInt() {
      return intValue;
    }
  }

}
