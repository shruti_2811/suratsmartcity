/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Raphaël Bussa
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package in.smc.sscdl.Utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by raphaelbussa on 22/06/16.
 */
public class PermissionManager {

    public interface AskagainCallback {

        void showRequestPermission(AskagainCallback.UserResponse response);

        interface UserResponse {
            void result(boolean askagain);
        }

    }

    public interface FullCallback {

        void result(ArrayList<PermissionEnum> permissionsGranted, ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever, ArrayList<PermissionEnum> permissionsAsked);

    }


    @SuppressLint("InlinedApi")
    public enum PermissionEnum {

        BODY_SENSORS(Manifest.permission.BODY_SENSORS),
        READ_CALENDAR(Manifest.permission.READ_CALENDAR),
        WRITE_CALENDAR(Manifest.permission.WRITE_CALENDAR),
        READ_CONTACTS(Manifest.permission.READ_CONTACTS),
        WRITE_CONTACTS(Manifest.permission.WRITE_CONTACTS),
        GET_ACCOUNTS(Manifest.permission.GET_ACCOUNTS),
        READ_EXTERNAL_STORAGE(Manifest.permission.READ_EXTERNAL_STORAGE),
        WRITE_EXTERNAL_STORAGE(Manifest.permission.WRITE_EXTERNAL_STORAGE),
        ACCESS_FINE_LOCATION(Manifest.permission.ACCESS_FINE_LOCATION),
        ACCESS_COARSE_LOCATION(Manifest.permission.ACCESS_COARSE_LOCATION),
        RECORD_AUDIO(Manifest.permission.RECORD_AUDIO),
        READ_PHONE_STATE(Manifest.permission.READ_PHONE_STATE),
        CALL_PHONE(Manifest.permission.CALL_PHONE),
        READ_CALL_LOG(Manifest.permission.READ_CALL_LOG),
        WRITE_CALL_LOG(Manifest.permission.WRITE_CALL_LOG),
        ADD_VOICEMAIL(Manifest.permission.ADD_VOICEMAIL),
        USE_SIP(Manifest.permission.USE_SIP),
        PROCESS_OUTGOING_CALLS(Manifest.permission.PROCESS_OUTGOING_CALLS),
        CAMERA(Manifest.permission.CAMERA),
        SEND_SMS(Manifest.permission.SEND_SMS),
        RECEIVE_SMS(Manifest.permission.RECEIVE_SMS),
        READ_SMS(Manifest.permission.READ_SMS),
        RECEIVE_WAP_PUSH(Manifest.permission.RECEIVE_WAP_PUSH),
        RECEIVE_MMS(Manifest.permission.RECEIVE_MMS),

        GROUP_CALENDAR(Manifest.permission_group.CALENDAR),
        GROUP_CAMERA(Manifest.permission_group.CAMERA),
        GROUP_CONTACTS(Manifest.permission_group.CONTACTS),
        GROUP_LOCATION(Manifest.permission_group.LOCATION),
        GROUP_MICROPHONE(Manifest.permission_group.MICROPHONE),
        GROUP_PHONE(Manifest.permission_group.PHONE),
        GROUP_SENSORS(Manifest.permission_group.SENSORS),
        GROUP_SMS(Manifest.permission_group.SMS),
        GROUP_STORAGE(Manifest.permission_group.STORAGE);

        private final String permission;

        PermissionEnum(String permission) {
            this.permission = permission;
        }

        public static PermissionEnum fromManifestPermission( String value) {
            for (PermissionEnum permissionEnum : PermissionEnum.values()) {
                if (value.equalsIgnoreCase(permissionEnum.permission)) {
                    return permissionEnum;
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return permission;
        }

    }

    public class PermissionConstant {

        public final static int KEY_PERMISSION = 100;

    }

    public class PermissionException extends Exception {

        public PermissionException(String message) {
            super(message);
        }

    }


    public interface SimpleCallback {

        void result(boolean allPermissionsGranted);

    }


    private static PermissionManager instance;

    private Context context;

    private FullCallback fullCallback;
    private SimpleCallback simpleCallback;
    private AskagainCallback askagainCallback;

    private boolean askagain = false;

    private ArrayList<PermissionEnum> permissions;
    private ArrayList<PermissionEnum> permissionsGranted;
    private ArrayList<PermissionEnum> permissionsDenied;
    private ArrayList<PermissionEnum> permissionsDeniedForever;
    private ArrayList<PermissionEnum> permissionToAsk;

    /**
     * @param context current context
     * @return current instance
     */
    public static PermissionManager with(Context context) {
        if (instance == null) {
            instance = new PermissionManager();
        }
        instance.init(context);
        return instance;
    }

    public static void handleResult(int requestCode, String[] permissions, int[] grantResults) {
        if (instance == null) return;
        if (requestCode == PermissionConstant.KEY_PERMISSION) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    instance.permissionsGranted.add(PermissionEnum.fromManifestPermission(permissions[i]));
                } else {
                    boolean permissionsDeniedForever = ActivityCompat.shouldShowRequestPermissionRationale((Activity) instance.context, permissions[i]);
                    if (!permissionsDeniedForever) {
                        instance.permissionsDeniedForever.add(PermissionEnum.fromManifestPermission(permissions[i]));
                    }
                    instance.permissionsDenied.add(PermissionEnum.fromManifestPermission(permissions[i]));
                    instance.permissionToAsk.add(PermissionEnum.fromManifestPermission(permissions[i]));
                }
            }
            if (instance.permissionToAsk.size() != 0 && instance.askagain) {
                instance.askagain = false;
                if (instance.askagainCallback != null && instance.permissionsDeniedForever.size() != instance.permissionsDenied.size()) {
                    instance.askagainCallback.showRequestPermission(new AskagainCallback.UserResponse() {
                        @Override
                        public void result(boolean askagain) {
                            if (askagain) {
                                instance.ask();
                            } else {
                                instance.showResult();
                            }
                        }
                    });
                } else {
                    instance.ask();
                }
            } else {
                instance.showResult();
            }
        }
    }

    private void init(Context context) {
        this.context = context;
    }

    /**
     * @param permissions an array of permission that you need to ask
     * @return current instance
     */
    public PermissionManager permissions(ArrayList<PermissionEnum> permissions) {
        this.permissions = new ArrayList<>();
        this.permissions.addAll(permissions);
        return this;
    }

    /**
     * @param permission permission you need to ask
     * @return current instance
     */
    public PermissionManager permission(PermissionEnum permission) {
        this.permissions = new ArrayList<>();
        this.permissions.add(permission);
        return this;
    }

    /**
     * @param permissions permission you need to ask
     * @return current instance
     */
    public PermissionManager permission(PermissionEnum... permissions) {
        this.permissions = new ArrayList<>();
        Collections.addAll(this.permissions, permissions);
        return this;
    }

    /**
     * @param askagain ask again when permission not granted
     * @return current instance
     */
    public PermissionManager askagain(boolean askagain) {
        this.askagain = askagain;
        return this;
    }

    public PermissionManager callback(FullCallback fullCallback) {
        this.simpleCallback = null;
        this.fullCallback = fullCallback;
        return this;
    }

    public PermissionManager callback(SimpleCallback simpleCallback) {
        this.fullCallback = null;
        this.simpleCallback = simpleCallback;
        return this;
    }

    public PermissionManager askagainCallback(AskagainCallback askagainCallback) {
        this.askagainCallback = askagainCallback;
        return this;
    }

    public void ask() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initArray();
            String[] permissionToAsk = permissionToAsk();
            if (permissionToAsk.length == 0) {
                showResult();
            } else {
                ActivityCompat.requestPermissions((Activity) context, permissionToAsk, PermissionConstant.KEY_PERMISSION);
            }
        } else {
            initArray();
            permissionsGranted.addAll(permissions);
            showResult();
        }
    }

    /**
     * @return permission that you realy need to ask
     */

    private String[] permissionToAsk() {
        ArrayList<String> permissionToAsk = new ArrayList<>();
        for (PermissionEnum permission : permissions) {
            if (!PermissionUtils.isGranted(context, permission)) {
                permissionToAsk.add(permission.toString());
            } else {
                permissionsGranted.add(permission);
            }
        }
        return permissionToAsk.toArray(new String[permissionToAsk.size()]);
    }

    /**
     * init permissions ArrayList
     */
    private void initArray() {
        this.permissionsGranted = new ArrayList<>();
        this.permissionsDenied = new ArrayList<>();
        this.permissionsDeniedForever = new ArrayList<>();
        this.permissionToAsk = new ArrayList<>();
    }

    private void showResult() {
        if (simpleCallback != null)
            simpleCallback.result(permissionToAsk.size() == permissionsGranted.size());
        if (fullCallback != null)
            fullCallback.result(permissionsGranted, permissionsDenied, permissionsDeniedForever, permissions);
    }



    /********************Start PermissionUtils *************************/
    public static class PermissionUtils {

        public static boolean isGranted(Context context, PermissionEnum permission) {
            return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || ContextCompat.checkSelfPermission(context, permission.toString()) == PackageManager.PERMISSION_GRANTED;
        }

        public static boolean isGranted(Context context, PermissionEnum... permission) {
            for (PermissionEnum permissionEnum : permission) {
                if (!isGranted(context, permissionEnum)) {
                    return false;
                }
            }
            return true;
        }

        public static Intent openApplicationSettings(String packageName) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + packageName));
                return intent;
            }
            return new Intent();
        }

        public static void openApplicationSettings(Context context, String packageName) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                Intent intent = openApplicationSettings(packageName);
                context.startActivity(intent);
            }
        }

    }
    /******************** END PermissionUtils *************************/

}
