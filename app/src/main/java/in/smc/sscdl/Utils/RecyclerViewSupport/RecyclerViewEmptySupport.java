package in.smc.sscdl.Utils.RecyclerViewSupport;

/**
 * Created by Parth patel on 25/05/2016.
 */

import android.content.Context;

import android.util.AttributeSet;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.joanzapata.iconify.widget.IconTextView;


public class RecyclerViewEmptySupport extends RecyclerView {

    View emptyView;
    IconTextView textView;


    final AdapterDataObserver observer = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfEmpty();
        }
    };

    public RecyclerViewEmptySupport(Context context) {
        super(context);
    }

    public RecyclerViewEmptySupport(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewEmptySupport(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void checkIfEmpty() {

        if (emptyView != null) {
            if (getAdapter() != null) {
                emptyView.setVisibility(getAdapter().getItemCount() > 0 ? GONE : VISIBLE);
            } else {
                emptyView.setVisibility(VISIBLE);
            }
        }
    }


    @Override
    public void swapAdapter(Adapter adapter, boolean removeAndRecycleExistingViews) {

        final Adapter oldAdapter = getAdapter();

        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }

        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }

        super.swapAdapter(adapter, removeAndRecycleExistingViews);
        checkIfEmpty();

    }

    @Override
    public void setAdapter(Adapter adapter) {

        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);

        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }
    }

    public void setEmptyView(View emptyView, String str) {
        this.emptyView = emptyView;
        textView = (IconTextView) emptyView;
        textView.setText(str);

        checkIfEmpty();
    }

    public IconTextView getTextView() {
        return textView;
    }

    public void setTextView(IconTextView textView) {
        this.textView = textView;
    }
}
